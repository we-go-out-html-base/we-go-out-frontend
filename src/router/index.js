// aqui, trago as rotas que preciso acessar

import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import EventosView from '../views/EventosView.vue'
import PoliticaCookiesView from '../views/PoliticaCookiesView.vue'
import PoliticaPrivacidadeView from '../views/PoliticaPrivacidadeView.vue'
import NaoEncontradaView from '../views/NaoEncontradaView.vue';
import DetalheEventoView from '../views/DetalheEventoView.vue';
import DetalheArtistaView from '../views/DetalheArtistaView.vue';
import DetalheLancamentoView from '../views/DetalheLancamentoView.vue';
import DetalheNoticiaView from '../views/DetalheNoticiaView.vue';
import TogethersView from '../views/TogethersView.vue';
import NoticiasView from '../views/NoticiasView.vue';
import LancamentosView from '../views/LancamentosView.vue';
import ArtistasView from '../views/ArtistasView.vue';
import BuscaView from '../views/BuscaView.vue';
import CriarEventoView from '../views/CriarEventoView.vue';
import ConsultaView from '../views/ConsultaView.vue';
import EsperaView from '../views/EsperaView.vue';
import ChaoView from '../views/ChaoView.vue';

//const domainsWithWaitPage = ['wegoout.com.br', 'localhost'];
const domainsWithWaitPage = ['wgo-temp.com.br'];

const currentDomain = window.location.hostname;

let waiting = domainsWithWaitPage.includes(currentDomain);

import RedirectMapping from '@/data/massive-redirects.json?31102023';

const redirectRoutes = RedirectMapping.map(mapping => ({
  path: mapping.oldUrl,
  redirect: mapping.newUrl,
}));

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: { title: 'Dicas de Festas, Festivais de Música e Viagens' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/espera',
    name: 'espera',
    component: EsperaView,
    meta: { title: 'Vem aí o novo site' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/chao',
    name: 'chao',
    component: ChaoView,
    meta: { title: 'Chão' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/sobre',
    name: 'sobre',
    component: AboutView,
     meta: { title: 'Sobre' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/consulta',
    name: 'consulta',
    component: ConsultaView,
    meta: { title: 'Consulta' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/criar-evento',
    name: 'criar-evento',
    component: CriarEventoView,
    meta: { title: 'Criar evento' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/busca',
    name: 'busca',
    component: BuscaView,
    meta: { title: 'Busca' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/together',
    name: 'togethers',
    component: TogethersView,
    meta: { title: 'Togethers' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/artistas',
    name: 'artistas',
    component: ArtistasView,
    meta: { title: 'Artistas' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/eventos',
    name: 'eventos',
    component: EventosView,
    meta: { title: 'Eventos' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/lancamentos',
    name: 'lancamentos',
    component: LancamentosView,
    meta: { title: 'Lançamentos' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/noticias',
    name: 'noticias',
    component: NoticiasView,
    meta: { title: 'Noticias' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/politica-de-cookies',
    name: 'cookies',
    component: PoliticaCookiesView,
    meta: { title: 'Política de Cookies' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/politica-de-privacidade',
    name: 'privacidade',
    component: PoliticaPrivacidadeView,
    meta: { title: 'Política de Privacidade' },
    beforeEnter: esperaGuard,
  },
  {
    path: '/eventos/:slug',
    name: 'evento',
    component: DetalheEventoView,
    meta: { dynamicTitle: true },
    beforeEnter: esperaGuard,
  },
  {
    path: '/artistas/:slug',
    name: 'artista',
    component: DetalheArtistaView,
    meta: { dynamicTitle: true },
    beforeEnter: esperaGuard,
  },
  {
    path: '/noticias/:slug',
    name: 'noticia',
    component: DetalheNoticiaView,
    meta: { dynamicTitle: true },
    beforeEnter: esperaGuard,
  },
  {
    path: '/lancamentos/:slug',
    name: 'lancamento',
    component: DetalheLancamentoView,
    meta: { dynamicTitle: true },
    beforeEnter: esperaGuard,
  },
  {
    path: '/:catchAll(.*)',
    name: 'naoencontrada',
    component: NaoEncontradaView,
    meta: { title: 'Página não encontrada' },
    beforeEnter: esperaGuard,
  },
  // redirects
  {
    path: '/agenda/:slug*/amp',
    redirect: to => {
      return `/eventos/${to.params.slug || ''}`;
    },
    beforeEnter: esperaGuard,
  },
  {
    path: '/agenda/:slug*',
    redirect: to => {
      return `/eventos/${to.params.slug || ''}`;
    },
    beforeEnter: esperaGuard,
  },
  {
    path: '/aaaaaaaaaaaaaaaaa',
    redirect: '/nnnnnnnnnnnnn',
  },
  ...redirectRoutes
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  base: process.env.BASE_URL,
  cache: false,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0, behavior: 'smooth' };
    }
  },
});

// método anterior à alteração
//router.beforeEach((to) => {
//  document.title = `We Go Out | ${to.meta.title || ''}`
//  
//})

// método criado para redirect de espera
router.beforeEach((to, from, next) => {
  if (waiting && to.path !== '/espera') {
    next('/espera'); // Redirecione para /espera se waiting for true e não estiver na página /espera
  } else {
    document.title = `${to.meta.title || ''}`;
    next(); // Continua a navegação normalmente se waiting for false ou na página /espera
  }
});

// método criado para redirect de espera
function esperaGuard(to, from, next) {
  if (waiting && to.path !== '/espera') {
    next('/espera');
  } else {
    next();
  }
}

router.beforeEach((to, from, next) => {
  const pathWithoutAmp = to.path.replace(/\/amp$/, ''); // Remove "/amp" se estiver presente
  if (pathWithoutAmp !== to.path) {
    next({ path: pathWithoutAmp, replace: true }); // Redireciona para a nova URL
  } else {
    next(); // Continua com a rota existente
  }
});


router.beforeEach((to, from, next) => {
  // Verifica se há parâmetros na URL e adiciona uma classe ao corpo
  if (Object.keys(to.query).length > 0) {
    document.body.classList.add('body-with-params');
  } else {
    document.body.classList.remove('body-with-params');
  }
  next();
});


export default router
