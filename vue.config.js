const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer:{
  	allowedHosts: 'all'
  	// port: process.env.port || 8080,
  	// disableHostCheck: true
  }
})
