function extractInstagramCode() {
    var inputField = document.getElementById('input-instagram-Field');
    var resultContainer = document.getElementById('result-instagram-Container');
    var errorDiv = document.getElementById('error-instagram');

    var instagramCode = inputField.value.trim();

    if (instagramCode === '') {
        errorDiv.textContent = 'Por favor, insira um código do Instagram.';
        resultContainer.textContent = '';
        return;
    }

    errorDiv.textContent = '';

    try {
        // Extrair o identificador do vídeo do código do Instagram
        var match = instagramCode.match(/instagram\.com\/(?:[^\/]+\/){1,2}([^\/?]+)/);
        if (match) {
            var videoId = match[1];
            var embedCode = `https://www.instagram.com/p/${videoId}/embed/`;

            // Exibir o código ajustado em um container
            resultContainer.value = `${embedCode}`;
        } else {
            throw new Error('Formato de código do Instagram inválido.');
        }
    } catch (error) {
        // Lidar com erros durante a extração
        errorDiv.textContent = 'Ocorreu um erro ao extrair o código do Instagram. Certifique-se de que o código fornecido seja válido.';
        resultContainer.textContent = '';
    }
}

function copyInstagramCodeToClipboard() {
    var resultContainer = document.getElementById('result-instagram-Container');
    var instagramCode = resultContainer.value.trim();

    if (instagramCode === '') {
        // Não há URL para copiar
        return;
    }

    navigator.clipboard.writeText(instagramCode)
        .then(function () {
            // Copiado com sucesso
            showCopySuccessAlert();
        })
        .catch(function (err) {
            // Tratamento de erro
            console.error('Erro ao copiar para a área de transferência:', err);
        });
}

// Função para exibir a div de alerta
function showCopySuccessAlert() {
    // Criando a div de alerta
    var alertDiv = document.createElement('div');
    alertDiv.classList.add('alert');

    // Criando um parágrafo para a mensagem
    var alertMessage = document.createElement('p');
    alertMessage.textContent = 'URL copiada para a área de transferência!';

    // Adicionando o parágrafo dentro da div de alerta
    alertDiv.appendChild(alertMessage);

    // Adicionando a div de alerta ao corpo do documento
    document.body.appendChild(alertDiv);

    // Removendo a div de alerta após alguns segundos
    setTimeout(function () {
        document.body.removeChild(alertDiv);
    }, 2000); // Remove a div após 2 segundos
}
