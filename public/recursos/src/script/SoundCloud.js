function extractSoundCloudCode() {
    var inputField = document.getElementById('input-soundCloud-Field');
    var resultContainer = document.getElementById('result-soundCloud-Container');
    var errorDiv = document.getElementById('error-soundCloud');

    var soundCloudCode = inputField.value.trim();

    if (soundCloudCode === '') {
        errorDiv.textContent = 'Por favor, insira um código do SoundCloud.';
        resultContainer.textContent = '';
        return;
    }

    errorDiv.textContent = '';

    try {
        // Extrair a URL do SoundCloud do código fornecido
        var soundCloudUrl = soundCloudCode.match(/src=["'](https:\/\/w.soundcloud.com\/player[^"']+)["']/)[1];

        // Exibir o código ajustado em um container
        resultContainer.value = `${soundCloudUrl}`;
    } catch (error) {
        // Lidar com erros durante a extração
        errorDiv.textContent = 'Ocorreu um erro ao extrair o código do SoundCloud. Certifique-se de que o código fornecido seja válido.';
        resultContainer.textContent = '';
    }
}

function copySoundCloudCodeToClipboard() {
    var resultContainer = document.getElementById('result-soundCloud-Container');
    var soundCloudUrl = resultContainer.value.trim();

    if (soundCloudUrl === '') {
        // Não há URL para copiar
        return;
    }

    navigator.clipboard.writeText(soundCloudUrl)
        .then(function () {
            // Copiado com sucesso
            showCopySuccessAlert();
        })
        .catch(function (err) {
            // Tratamento de erro
            console.error('Erro ao copiar para a área de transferência:', err);
        });
}

// Função para exibir a div de alerta
function showCopySuccessAlert() {
    // Criando a div de alerta
    var alertDiv = document.createElement('div');
    alertDiv.classList.add('alert');

    // Criando um parágrafo para a mensagem
    var alertMessage = document.createElement('p');
    alertMessage.textContent = 'URL copiada para a área de transferência!';

    // Adicionando o parágrafo dentro da div de alerta
    alertDiv.appendChild(alertMessage);

    // Adicionando a div de alerta ao corpo do documento
    document.body.appendChild(alertDiv);

    // Removendo a div de alerta após alguns segundos
    setTimeout(function () {
        document.body.removeChild(alertDiv);
    }, 2000); // Remove a div após 2 segundos
}
