function extractMixCloudCode() {
    var inputField = document.getElementById('input-mixCloud-Field');
    var resultContainer = document.getElementById('result-mixCloud-Container');
    var errorDiv = document.getElementById('error-mixCloud');

    var mixcloudCode = inputField.value.trim();

    if (mixcloudCode === '') {
        errorDiv.textContent = 'Por favor, insira um código do Mixcloud.';
        resultContainer.textContent = '';
        return;
    }

    errorDiv.textContent = '';

    try {
        // Extrair a URL do Mixcloud do código fornecido
        var mixcloudUrl = mixcloudCode.match(/src=["'](https:\/\/player-widget\.mixcloud\.com[^"']+)["']/)[1];

        // Exibir o código ajustado em um container
        resultContainer.value = `${mixcloudUrl}`;
    } catch (error) {
        // Lidar com erros durante a extração
        errorDiv.textContent = 'Ocorreu um erro ao extrair o código do Mixcloud. Certifique-se de que o código fornecido seja válido.';
        resultContainer.textContent = '';
    }
}

function copyMixCloudCodeToClipboard() {
    var resultContainer = document.getElementById('result-mixCloud-Container');
    var mixcloudUrl = resultContainer.value.trim();

    if (mixcloudUrl === '') {
        // Não há URL para copiar
        return;
    }

    navigator.clipboard.writeText(mixcloudUrl)
        .then(function () {
            // Copiado com sucesso
            showCopySuccessAlert();
        })
        .catch(function (err) {
            // Tratamento de erro
            console.error('Erro ao copiar para a área de transferência:', err);
        });
}

function showCopySuccessAlert() {
    // Criando a div de alerta
    var alertDiv = document.createElement('div');
    alertDiv.classList.add('alert');

    // Criando um parágrafo para a mensagem
    var alertMessage = document.createElement('p');
    alertMessage.textContent = 'URL copiada para a área de transferência!';

    // Adicionando o parágrafo dentro da div de alerta
    alertDiv.appendChild(alertMessage);

    // Adicionando a div de alerta ao corpo do documento
    document.body.appendChild(alertDiv);

    // Removendo a div de alerta após alguns segundos
    setTimeout(function () {
        document.body.removeChild(alertDiv);
    }, 2000); // Remove a div após 2 segundos
}

