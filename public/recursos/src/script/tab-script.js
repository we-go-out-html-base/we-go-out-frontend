document.addEventListener('DOMContentLoaded', () => {
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function removeTabClasses() {
        document.body.classList.forEach(className => {
            if (className.endsWith('-tab-active')) {
                document.body.classList.remove(className);
            }
        });
    }

    function updateFromURL() {
        var tabs = document.querySelectorAll('.tabs h4');
        tabs.forEach(tab => tab.classList.remove('active'));

        removeTabClasses();

        var spotify = getParameterByName('spotify-tab');
        var spotifyTab = document.getElementById('spotify-tab');
        var soudcloud = getParameterByName('soundCloud-tab');
        var soudcloudTab = document.getElementById('soundCloud-tab');
        var mixcloud = getParameterByName('mixCloud-tab');
        var mixcloudTab = document.getElementById('mixCloud-tab');
        var insta = getParameterByName('insta-tab');
        var instaTab = document.getElementById('insta-tab');
        var facebook = getParameterByName('facebook-tab');
        var facebookTab = document.getElementById('facebook-tab');

        if (spotify === 'true' && spotifyTab) {
            document.body.classList.add('spotify-tab-active');
            spotifyTab.classList.add('active');
        }
        else if (soudcloud === 'true' && soudcloudTab) {
            document.body.classList.add('soundCloud-tab-active');
            soudcloudTab.classList.add('active');
        }
        else if (mixcloud === 'true' && mixcloudTab) {
            document.body.classList.add('mixCloud-tab-active');
            mixcloudTab.classList.add('active');
        }
        else if (insta === 'true' && instaTab) {
            document.body.classList.add('insta-tab-active');
            instaTab.classList.add('active');
        }
        else if(facebook === 'true' && facebookTab) {
            document.body.classList.add('facebook-tab-active');
            facebookTab.classList.add('active');
        }else{
            document.body.classList.add('insta-tab-active');
            instaTab.classList.add('active');
        }
    }

    function handleTabClick(tabId) {
        var tabs = document.querySelectorAll('.tabs h4');
        tabs.forEach(tab => tab.classList.remove('active'));

        removeTabClasses();

        document.body.classList.add(`${tabId}-active`);
        document.getElementById(tabId).classList.add('active');

        history.pushState({}, '', `?${tabId}=true`);
    }

    const tabs = document.querySelectorAll('.tabs h4');
    tabs.forEach(tab => {
        tab.addEventListener('click', function() {
            var tabId = this.id;
            handleTabClick(tabId);
        });
    });

    window.onpopstate = function() {
        updateFromURL();
    };

    updateFromURL(); // Atualização inicial baseada na URL
});
